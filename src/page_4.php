<?php
	$conn = mysql_connect('91.184.10.91', 'mysqlgratisgolf', 'g0lfp0l1s');
	mysql_select_db('gratisgolfpolis', $conn);
	
	$result = mysql_query("SELECT * FROM akties WHERE name LIKE ('".$_POST["id"]."') AND entrycode LIKE ('".$_POST["entry"]."')");
	
	if(mysql_num_rows($result) == 0) {
		header("Location: index.php");  
		exit; 	
	} else {
		$row = mysql_fetch_array($result);
		$shop = $row["aanbieder"];
	
		$result = mysql_query("SELECT * FROM aanbieders WHERE id ='".$shop."'");
		$row = mysql_fetch_array($result);
		
		$minLeeftijdAanvrager = (date("Y")-12);
		$daymonth = ((date("n")*100)+date("j"));
		
		$termijnAanmelding = 30;
		$maxAankoopdat = (date("Y"));
		$aankoopmonth = ((date("n")*100)+(date("j")-(70+$termijnAanmelding)));
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title>GRATIS-GOLFPOLIS.nl</title>
 	    <link href="golfpolis_styling.css" rel="stylesheet" type="text/css" />
    
	    <script>
			function getPC(pccijfers, pcletters, adres, plaats) {
				getPostcode("postcodediv", "/getPostcode.php?pcletters="+document.getElementById(pcletters).value+"&pccijfers="+document.getElementById(pccijfers).value, adres, plaats);
			}
			
		
			function getPostcode(id, url, adres, plaats){
				var xmlHttp;
				try {// Firefox, Opera 8.0+, Safari
					xmlHttp = new XMLHttpRequest();		
				} catch (e) {// Internet Explorer
						try {
						xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						try {
							xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
						} catch (e) {
							return false;
						}
					}
				}
					
				xmlHttp.onreadystatechange = function(){
					if (xmlHttp.readyState == 4) {
						var respText = xmlHttp.responseText.split('<body>');
						elem.innerHTML = respText[1].split('</body>')[0];
						
						document.getElementById(adres).value = document.getElementById("street").value;
						document.getElementById(plaats).value = document.getElementById("city").value;
					}
				}
			
			
				var elem = document.getElementById(id);
				if (!elem) {
					return;
				}
				
				xmlHttp.open("GET", url, true);
				xmlHttp.send(null);
			}
			
			
	function validate(){
		result = true;
		
		result = result && validRequiredText(document.forms[0].achternaam.value, 'uw achternaam');
		result = result && validRequiredText(document.forms[0].voorletters.value, 'uw voorletters');
		result = result && validRequiredOption(document.forms[0].geslacht, 'uw geslacht');
		result = result && validRequiredText(document.forms[0].geboorte_dag.value, 'uw geboortedatum');
		result = result && validRequiredText(document.forms[0].geboorte_maand.value, 'uw geboortedatum');
		result = result && validRequiredText(document.forms[0].geboorte_jaar.value, 'uw geboortedatum');
		result = result && geboortedatumIsValid();
		result = result && validateGeboorteDatumMin();
		result = result && validRequiredText(document.forms[0].pccijfers.value, 'uw postcode');
		result = result && validRequiredText(document.forms[0].pcletters.value, 'uw postcode');
		result = result && validRequiredText(document.forms[0].huisnummer.value, 'uw huisnummer');
		result = result && validRequiredText(document.forms[0].adres.value, 'uw adres');
		result = result && validRequiredText(document.forms[0].woonplaats.value, 'uw woonplaats');
		result = result && validRequiredText(document.forms[0].telefoon_prive.value, 'uw telefoonnr.');
		result = result && validRequiredText(document.forms[0].emailadres.value, 'een geldig mailadres');
		result = result && checkEmail(document.forms[0].emailadres);
		result = result && validRequiredText(document.forms[0].gekochte_clubs.value, 'het aantal clubs in');
		result = result && validRequiredText(document.forms[0].totaalbedrag.value, 'het totaalbedrag in');
		result = result && validRequiredText(document.forms[0].kassabon_nummer.value, 'uw kassabonnr.');
		result = result && validRequiredText(document.forms[0].aankoopdatum_dag.value, 'de aankoopdatum');
		result = result && validRequiredText(document.forms[0].aankoopdatum_maand.value, 'de aankoopdatum');
		result = result && validRequiredText(document.forms[0].aankoopdatum_jaar.value, 'de aankoopdatum');
		result = result && aankoopdatumIsValid();
		result = result && validateAankoopdatumMin();
		
		return result;
	}


	function validRequiredText(field, name) {
		var isEmpty = false;
		if(field.length<1) {
			alert("Vul s.v.p. " + name + " in");
			isEmpty =true;
		}
		
		return !isEmpty;
	}
	
	
	function validRequiredOption(field, name) {
		var isEmpty = false;
		if((!field[0].checked&&!field[1].checked)) {
			alert("Vul s.v.p. " + name + " in");
			isEmpty =true;
		}
		
		return !isEmpty;
	}
	
	
	function checkEmail(obj)
	{
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var address = obj.value;
		if(reg.test(address) == false) {
			alert("Vul s.v.p. een geldig emailadres in");
			return false;
		} else {
			return true;
		}
	}	
	
	
	function checkNumInp(code)
	{
		result = true;
		if(code>=48&&code<=57)
			result = true;
					
		if(!result)
		{
			if(  code==8
			  || code==9
			  || code==46
			  || code==13
			  || code==37
			  || code==38
			  || code==39
			  || code==40
			  || code==16
			  || code==27 )
					   
			result = true;
		}
				
		return result;
	}
	
	
	function geboortedatumIsValid()
	{	
		gebjaar = (eval(document.forms[0].geboorte_jaar.value)*1);
		gebmaand = ((eval(document.forms[0].geboorte_maand.value)*1)-1);
		gebdag = (eval(document.forms[0].geboorte_dag.value)*1);
					
		var gebDate=new Date();
		gebDate.setFullYear(gebjaar,gebmaand,gebdag);						
		if(gebDate.getDate()!=gebdag
		  ||gebmaand<0
		  ||gebmaand>11)
		{
			alert("Geboortedatum is een ongeldige datum");
			try {
				document.forms[0].geboorte_dag.focus();
			} catch (e) {}
			
			return false;	
		}
				
		return true;
	}	
	
	
	function validateGeboorteDatumMin()
	{
		result = true;
		gebjaar = eval(document.forms[0].geboorte_jaar.value);
		daymonth = ((eval(document.forms[0].geboorte_maand.value)*100) + (eval(document.forms[0].geboorte_dag.value)*1) -70);
					
		if((gebjaar><?=$minLeeftijdAanvrager?>)
				||(gebjaar==<?=$minLeeftijdAanvrager?>&&daymonth><?=$daymonth?>))	
		{
			result = false;
			alert("De minimum leeftijd voor aanvrager is 12 jaar");
			return false;
		}
					
		return true;
	}	
	
	
	function aankoopdatumIsValid()
	{	
		gebjaar = (eval(document.forms[0].aankoopdatum_jaar.value)*1);
		gebmaand = ((eval(document.forms[0].aankoopdatum_maand.value)*1)-1);
		gebdag = (eval(document.forms[0].aankoopdatum_dag.value)*1);
					
		var gebDate=new Date();
		gebDate.setFullYear(gebjaar,gebmaand,gebdag);						
		if(gebDate.getDate()!=gebdag
		  ||gebmaand<0
		  ||gebmaand>11)
		{
			alert("Aankoopdatum is een ongeldige datum");
			try {
				document.forms[0].aankoopdatum_dag.focus();
			} catch (e) {}
			
			return false;	
		}
				
		return true;
	}	
	


	function validateAankoopdatumMin()
	{
		result = true;
		gebjaar = eval(document.forms[0].aankoopdatum_jaar.value);
		daymonth = ((eval(document.forms[0].aankoopdatum_maand.value)*100) + (eval(document.forms[0].aankoopdatum_dag.value)*1));
		
		if((gebjaar<<?=$maxAankoopdat?>)
				||(gebjaar==<?=$maxAankoopdat?>&&daymonth<<?=$aankoopmonth?>))	
		{
			result = false;
			alert("Aanmelding is slechts mogelijk tot 10 dagen na de aankoopdatum");
			return false;
		}
		if((gebjaar><?=$maxAankoopdat?>)
				||(gebjaar==<?=$maxAankoopdat?>&&((daymonth-70)-<?=$termijnAanmelding?>)><?=$aankoopmonth?>)){
			result = false;
			alert("De aankoopdatum kan niet in de toekomst liggen");
			return false;
		}
		return true;
	}	
</script>

</head>

<body>
<div id="postcodediv"></div>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><img src="images/logo_golfpolis.jpg" alt="Logo gratis-golfpolis.nl" width="780" height="110" /><br />
    <br /></td>
  </tr>
  <tr>
    <td width="130">&nbsp;</td>
    <td width="520" class="Frame"><span style="font-weight: bold; color: #0B5A00;">AANMELDFORMULIER GRATIS VERZEKERING</span><br />
      <br />
<?= $row["aanvraag"] ?>
<!--<br />
Wij adviseren u om dit certificaat na ontvangst meteen te printen en hierna -samen met uw kassabon- goed op te bergen! Mocht u geen certicaat ontvangen bel dan op de eerstvolgende werkdag tussen 10.00u en 16.00u even met:
<br />
      <br />
      <div align="center"><span style="font-weight: bold; color: #0B5A00;">INTER-GOLF ASSURANTI&Euml;N</span><br />
      tel. 0900 - 123 GOLF (=> 0900 - 123 4653 / 0,05ct. p.min.)
      </div>-->
      <strong><br />
      Voorwaarden</strong><br />
Voor een exacte omschrijving van de dekking verwijzen wij u naar de voor deze gratis verzekering geldende voorwaarden die u hier kunt downloaden. In deze voorwaarden staat precies omschreven wat u in geval van verlies of diefstal moet doen, hoe u de schade moet melden, etc. &gt; <img src="images/file_acrobat.gif" width="16" height="16" align="absmiddle" /> <a href="/GRATIS-GOLFPOLIS VOORWAARDEN GGP-HDW.042011.pdf" target="_blank">Download hier de voorwaarden, PDF</a><br />
<br />
<form action="page_5.php" method="post" onsubmit="return validate();">
	<input type="hidden" name="id" value="<?=$_POST["id"]?>"/>
    <input type="hidden" name="entry" value="<?=$_POST["entry"]?>"/>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr class="Tbl_odd">
    <td colspan="2" class="Tbl_even"><span style="font-weight: bold; color: #0B5A00;">AANMELDFORMULIER GRATIS VERZEKERING</span></td>
    </tr>
  <tr class="Tbl_odd">
    <td width="200" class="Tbl_even"><strong>Achternaam</strong></td>
    <td class="Tbl_even"><input type="text" name="achternaam" id="achternaam" size="25" tabindex="1" /></td>
  </tr>
  <tr class="Tbl_even">
    <td width="200" class="Tbl_even"><strong>Voorletters</strong></td>
    <td class="Tbl_even"><input name="voorletters" type="text" id="voorletters" size="10" tabindex="2" /></td>
  </tr>
  <tr>
    <td width="200" valign="top" class="Tbl_even"><strong>Geslacht</strong></td>
    <td class="Tbl_even">
      <label>
        <input type="radio" name="geslacht" value="man" id="geslacht_0"  tabindex="3"/>
        man</label>
      <br />
      <label>
        <input type="radio" name="geslacht" value="vrouw" id="geslacht_1"  tabindex="4"/>
        vrouw</label>
      <br />      </td>
  </tr>
  <tr class="Tbl_even">
    <td width="200" class="Tbl_even"><strong>Geboortedatum</strong></td>
    <td class="Tbl_even"><input name="geboorte_dag" type="text" id="geboorte_dag" size="1" maxlength="2"  tabindex="5"/>
      -
        <input name="geboorte_maand" type="text" id="geboorte_maand" size="1" maxlength="2"  tabindex="6"/>
        -
        <input name="geboorte_jaar" type="text" id="geboorte_jaar" size="2" maxlength="4"  tabindex="7"/> 
        DD-MM-JJJJ</td>
  </tr>
  
  <tr>
    <td width="200" class="Tbl_even"><strong>Postcode</strong></td>
    <td class="Tbl_even">
    	<input name="pccijfers" type="text" id="pccijfers" size="3" maxlength="4" onkeypress="return checkNumInp(event.keyCode);" onblur="getPC('pccijfers','pcletters','adres','woonplaats');" style="width:30px;" tabindex="8"/>
    	<input name="pcletters" type="text" id="pcletters" size="2" maxlength="2" onblur="getPC('pccijfers','pcletters','adres','woonplaats');" style="width:20px;" tabindex="9"/>
    </td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Huisnummer</strong></td>
    <td class="Tbl_even"><input name="huisnummer" type="text" id="huisnummer" size="4" tabindex="10" /></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Adres</strong></td>
    <td class="Tbl_even"><input type="text" name="adres" id="adres" size="25" tabindex="11"/></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Woonplaats</strong></td>
    <td class="Tbl_even"><input type="text" name="woonplaats" id="woonplaats" size="25" tabindex="12"/></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Telefoonnr.</strong></td>
    <td class="Tbl_even"><input type="text" name="telefoon_prive" id="telefoon_prive" tabindex="13" /></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Emailadres</strong></td>
    <td class="Tbl_even"><input type="text" name="emailadres" id="emailadres" size="25" tabindex="14"/></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Aantal gekochte clubs</strong></td>
    <td class="Tbl_even"><input name="gekochte_clubs" type="text" id="gekochte_clubs" size="1" maxlength="2" onkeypress="return checkNumInp(event.keyCode);" tabindex="15"/></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Totaalbedrag</strong></td>
    <td class="Tbl_even"><input name="totaalbedrag" type="text" id="totaalbedrag" size="4" maxlength="5" style="width:40px;" onkeypress="return checkNumInp(event.keyCode);" tabindex="16"/><input type="text" value=",00" size="3" style="width:20px;" readonly="readonly"/> (-&gt; afronden op hele EURO's)</td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Kassabon nr.</strong></td>
    <td class="Tbl_even"><input type="text" name="kassabon_nummer" id="kassabon_nummer" tabindex="17" /></td>
  </tr>
  <tr>
    <td width="200" class="Tbl_even"><strong>Aankoopdatum</strong></td>
    <td class="Tbl_even"><input name="aankoopdatum_dag" type="text" id="aankoopdatum_dag" size="1" maxlength="2" tabindex="18" />
-
  <input name="aankoopdatum_maand" type="text" id="aankoopdatum_maand" size="1" maxlength="2" tabindex="19" />
-
<input name="aankoopdatum_jaar" type="text" id="aankoopdatum_jaar" size="2" maxlength="4" tabindex="20" />
DD-MM-JJJJ</td>
  </tr>
  <tr>
    <td colspan="2"><br />
      <div style="color: red; text-align: center;"><b>Heeft u ALLE velden ingevuld?<br />
        Klik dan hieronder om uw aanmelding te versturen.</b><br />
        <br />
          <input type="submit" name="submit" id="submit" value="Verstuur aanmelding" tabindex="21" />
      </div></td>
    </tr>
</table>
</form>
<br />
<br /></td>
    <td width="130">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><a href="contact.php" style="color:#000000;">contact</a> | <a href="privacy.php" style="color:#000000;">privacybeleid</a> | <a href="disclaimer.php" style="color:#000000;">disclaimer</a></td>
  </tr>
  <tr>
    <td colspan="3" class="Footer">&copy; 01-2013: INTER-GOLF ASSURANTIEN</td>
  </tr>
</table>
</body>
</html>
<?php
}
?>